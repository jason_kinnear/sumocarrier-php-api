<?php

namespace SumoCarrier;

class Transaction {

    private $server;
    private $id;
    private $apiKey;

    public function __construct(Server $server, $api_key = null, $transaction_id = null) {
        $this->server = $server;
        $this->id = $transaction_id;
        $this->apiKey = $api_key;
    }

    public function generate() {
        $response = $this->request($this->getServer()->transaction_id_url());
        if (!empty($response['transaction_id'])) {
            $this->id = $response['transaction_id'];
        }
        return trim($response['transaction_id']);
    }


    public function getAPIKey() {
        return $this->apiKey;
    }

    /**
     * @return mixed
     */
    public function getID() {
        return $this->id;
    }

    /**
     * @return Server
     */
    public function getServer() {
        return $this->server;
    }

    /**
     * @param Server $server
     *
     * @return Transaction
     */
    public function setServer($server) {
        $this->server = $server;
        return $this;
    }

    /**
     * @param mixed $id
     */
    protected function setID($id) {
        $this->id = $id;
    }

    protected function setAPIKey($api_key) {
        $this->apiKey = $api_key;
        return $this;
    }

    /**
     * Create a request
     *
     * @param $url
     * @param array $params
     * @return mixed|string
     */
    public function request($url, array $params = array( ), $object = false) {
        $url = rtrim($url, '?') . (empty($params) ? '' : ('?'.http_build_query($params)));
        $response = file_get_contents($url);
        $json = json_decode($response, !$object);
        if (is_array($json)) {
            return $json;
        }
        return $response;
    }


    static public function create(Server $server, $api_key = null, $transaction_id = null) {
        return new self($server, $api_key, $transaction_id);
    }


}